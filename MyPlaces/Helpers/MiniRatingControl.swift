//
//  MiniRaitingControl.swift
//  MyPlaces
//
//  Created by Владимир Свиридов on 17.12.2021.
//

import UIKit

class MiniRatingControl: UIStackView {
    
    // MARK: Properties
    
    var rating = 0
    
    private var ratingImages = [UIImageView]()
    
    var starSize: CGSize = CGSize(width: 22.0, height: 22.0)
    var starCount: Int = 5
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // Create and setup raiting on the view
    func setupRating(currentRating: Int) {
        
        // Clean rating befor update
        for imageView in ratingImages {
            removeArrangedSubview(imageView)
            imageView.removeFromSuperview()
        }

        ratingImages.removeAll()
        
        self.axis  = NSLayoutConstraint.Axis.horizontal
        self.distribution  = UIStackView.Distribution.equalSpacing
        self.alignment = UIStackView.Alignment.center
        self.spacing   = 1.0
        
        
        for index in 0..<starCount {
            
            let imageView = UIImageView()
            
            if index < currentRating {
                // Create image
                let filledImage = UIImage(named: "filledStar")
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
                imageView.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
                imageView.image = filledImage
                
                // Add the image to the stack
                self.addArrangedSubview(imageView)
                
                // Add the new image on the rating button array
                ratingImages.append(imageView)
            } else {
            // Create image
            let emptyImage = UIImage(named: "emptyStar")
            let imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
            imageView.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
            imageView.image = emptyImage

            // Add the image to the stack
            self.addArrangedSubview(imageView)

            // Add the new image on the rating button array
            ratingImages.append(imageView)
            }
        }
    }
}
